/* xfx
 * ------------------- 
 * Using sensors to control guitar fx.
 */
#include <NewPing.h>
#include <Keypad.h>
#include <SoftwareSerial.h>

// Extra serial connection for bluetooth communication
SoftwareSerial bt_serial(10, 11); // RX, TX (connect RX of Bluetooth module to pin 11 and TX to 10!)

// Joystick
const int JOYSTICK_X_PIN = 4;
const int JOYSTICK_Y_PIN = 5;
int joystick_x_value;
int joystick_y_value;

// Sonar
// pin tied to both trigger and echo pins on the ultrasonic sensor
const int SONAR_PIN = 9;
// Maximum distance to ping (in centimeters). Maximum sensor distance is ~400-500cm.
const int MAX_SONAR_DISTANCE = 50;
// NewPing setup of pin and maximum distance.
NewPing sonar(SONAR_PIN, SONAR_PIN, MAX_SONAR_DISTANCE); 

// Matrix keypad
const byte ROWS = 4;
const byte COLS = 3;
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {8, 7, 6, 5}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {4, 3, 2}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS );


void setup() {
    // Connect to the serial port
    Serial.begin(9600);

    // Connect to bluetooth dongle (the softserial port)
    Serial.println("Starting BlueTooth module...");
    bt_serial.begin(9600);
    delay(1000);
    bt_serial.print("AT");
    delay(1000);
    bt_serial.print("AT+VERSION");
    delay(1000);
    bt_serial.print("AT+PIN1234"); // Set pin to 1234 (which is also the factory default)
    delay(1000);
    bt_serial.print("AT+NAMExfx"); // Set the name of the arduino bluetooth module
    delay(1000);
    bt_serial.print("AT+BAUD4"); // Set baudrate to 9600
    delay(1000);
    Serial.println("Bluetooth module intialized");
}


void loop () {
    // Read joystick
    joystick_x_value = analogRead(JOYSTICK_X_PIN);
    joystick_y_value = analogRead(JOYSTICK_Y_PIN);

    // Write result to serial connection
    bt_serial.print("joystick:");
    bt_serial.print(joystick_x_value);
    bt_serial.print(",");
    bt_serial.println(joystick_y_value);

    // Read sonar
    // Sonar requires the delay to be at least 29ms
    delay(50);
    // Send ping, get ping time in microseconds (uS).
    unsigned int u_sec = sonar.ping(); 
    bt_serial.print("sonar:"); 
    // Convert ping time to distance (cm) and print (0 means > MAX_SONAR_DISTANCE)
    bt_serial.println(u_sec / US_ROUNDTRIP_CM); 

    // Matrix keypad
    char key = keypad.getKey();
    if (key != NO_KEY){
      bt_serial.print("keypad:"); 
      bt_serial.println(key);
    }
}
