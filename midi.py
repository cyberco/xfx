"""
Module for midi proxying.
"""
import pygame
import pygame.midi
import logging

logging.basicConfig(level=logging.DEBUG)

#midi_device_name = 'loopMIDI' # Windows
midi_device_name = 'IAC' # OSX 

class Midi(object):
    """Midi proxy.

    On initialization all MIDI devices will be discovered and assigned to properties
    of this instance (self.midi_out_0, self.midi_out_1, ..., self.midi_out_n)
    This class should be used as a context manager (for use in a 'with' statement).
    """

    def __enter__(self):
        pygame.init()
        pygame.midi.init()
        nr_devices = pygame.midi.get_count()
        if nr_devices == 0:
            raise Exception('No midi devices found')
        # find MIDI ports
        for device_id in range(nr_devices):
            # See: https://www.pygame.org/docs/ref/midi.html#pygame.midi.get_device_info
            device_info = pygame.midi.get_device_info(device_id)
            logging.debug('device:' + str(device_info))
            if device_info[3] != 1:
                # Not an output device
                continue
            name = device_info[1].decode('ascii')
            if name.startswith(midi_device_name):
                # set self.midi_out_0, self.midi_out_1, ..., self.midi_out_n
                loop_midi_nr = name[-1] if name[-1].isdigit() else '0'
                setattr(self, 'midi_out_' + loop_midi_nr, pygame.midi.Output(device_id, 0))
                logging.debug('Using midi output device: %s' % str(device_info))
        #enter has to return the object to use in the with...as statement explicitely
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if any([exc_type, exc_value, exc_traceback]):
            logging.debug('__exit__(%s, %s, %s)' % (exc_type, exc_value, exc_traceback))
        # Delete all midi_out devices
        for device_nr in range(10):
            try:
                midi_out = getattr(self, 'midi_out_' + device_nr)
                del midi_out
            except Exception:
                break
        pygame.midi.quit()
        logging.debug('midi output closed')
