"""Sensor. Arduino. Python. Reason. Rock."""

import os
import logging
import serial
from time import sleep
from midi import Midi as MidiProxy

# Sensor ranges
JOYSTICK_MAX_VALUE = 1024
SONAR_MAX_VALUE = 50  # 50 cm is the max distance the sonar sensor will return

#Reason MIDI controllers
REASON_MIDI_SCREAM_DAMAGE = 13
REASON_MIDI_SCREAM_BODY = 71
REASON_MIDI_ECHO_DELAY = 12
REASON_MIDI_ECHO_ROLL = 1
REASON_MIDI_ALLIGATOR_ON = 9
REASON_MIDI_THOR_PITCH = 39

# Map sensor to midi devices
JOYSTICK_x_MIDICONTROLLER = REASON_MIDI_SCREAM_DAMAGE
JOYSTICK_Y_MIDICONTROLLER = REASON_MIDI_SCREAM_BODY
ECHO_MIDICONTROLLER = REASON_MIDI_ECHO_DELAY
#ECHO_MIDICONTROLLER = REASON_MIDI_THOR_PITCH
KEYPAD_1_MIDICONTROLLER = REASON_MIDI_ECHO_ROLL
KEYPAD_2_MIDICONTROLLER = REASON_MIDI_ALLIGATOR_ON


# Keypad key toggles
class Keypad(object):
    key1_on = False
    key2_on = False

logging.basicConfig(level=logging.DEBUG)

# Diffentiate between Windows and Linux. Check Windows device manager for COM port
switcher = {'nt': 'COM6',
            'linux':'/dev/ttyUSB0',
            'posix': '/dev/cu.xfx-DevB', # OSX
            'darwin': '/dev/cu.xfx-DevB'
            }
serial_port = switcher.get(os.name)
logging.debug('OS: ' + str(os.name))
logging.debug('Using serial port: ' + str(serial_port))


def run():
    """Read from the serial connection and write to the MIDI device."""
    with serial.Serial(serial_port, 9600) as serial_connection, MidiProxy() as midi_proxy:
        logging.debug('proxy: ' + str(dir(midi_proxy)))
        for key, value in read_serial(serial_connection):
            write_midi(midi_proxy, key, value)


def read_serial(serial_connection):
    """Read from the serial connection

    args:
    serial_connectiohn - serial.Serial

    return key, value
    """
    while True:
        value = serial_connection.readline()
        if not value:
            logging.debug('Nothing read')
            continue
        value = value.decode('ascii')
        sensor, value = value.split(':')
        if sensor == 'joystick':
            result = handle_joystick(value)
            if result:
                yield 'joystick', result
        if sensor == 'sonar':
            result = handle_sonar(value)
            if result:
                yield 'sonar', result
        if sensor == 'keypad':
            result = handle_keypad(value)
            if result:
                yield 'keypad', result


def write_midi(midi_proxy, sensor, value):
    """Write to the midi device

    args:
    midi_proxy - midi.MIDI
    sensor - sensor
    value - value as read by sensor
    """
    if sensor == 'joystick':
        x, y = value
        #remap note to 0-127 range
        note_x = int(x / float(JOYSTICK_MAX_VALUE) * 127)
        note_y = int(y / float(JOYSTICK_MAX_VALUE) * 127)
        #send a control change (0xb0)
        midi_proxy.midi_out_1.write_short(0xb0, JOYSTICK_x_MIDICONTROLLER, note_x)
        midi_proxy.midi_out_1.write_short(0xb0, JOYSTICK_Y_MIDICONTROLLER, note_y)
    elif sensor == 'sonar':
        # remap to 0-127 range
        value = int((value / float(SONAR_MAX_VALUE)) * 127)
        midi_proxy.midi_out_2.write_short(0xb0, ECHO_MIDICONTROLLER, value)
    elif sensor == 'keypad':
        if value == '1':
            Keypad.key1_on = not Keypad.key1_on
            value = 0 if Keypad.key1_on else 127
            midi_proxy.midi_out_3.write_short(0xb0, KEYPAD_1_MIDICONTROLLER, value)
        elif value == '2':
            Keypad.key2_on = not Keypad.key2_on
            value = 1 if Keypad.key2_on else 2
            midi_proxy.midi_out_4.write_short(0xb0, KEYPAD_2_MIDICONTROLLER, value)


def handle_joystick(value):
    """Handle joystick events

    Args:
    value - String in format '123,434'

    Returns tuple (123, 434)
    """
    value = [v.strip() for v in value.split(',')]
    if len(value) != 2:
        return
    x, y = value
    try:
        x = int(x)
        y = int(y)
    except:
        return
    logging.debug('x:' + str(x) + int(x/10) * ' ' + '=')
    logging.debug('y:' + str(y) + int(y/10) * ' ' + '*')
    return (x, y)


def handle_sonar(value):
    """Handle sonar events.

    Args:
    value - String in format '123' indicating the distance the sonar measured.

    Returns int (<50) representing the distance in centimeters
    """
    value = value.strip()
    if not value:
        return
    try:
        value = int(value)
    except:
        return
    logging.debug('sonar:' + str(value) + value * ' ' + '>')
    return value


def handle_keypad(value):
    """Handle keypad events.

    Args:
    value - char of the key pressed (e.g. '1', '#').

    Returns char of the key pressed
    """
    value = value.strip()
    logging.debug('============keypad:' + str(value))
    if not value:
        return
    return value


if __name__ == '__main__':
    run()
