'''
Midi tests
'''
from midi import Midi as MidiProxy
from time import sleep


def test_send_midi():
    with MidiProxy() as midi:
        values = range(72, 76, 1) + range(76, 72, -1)
        for value in values:
            midi.midi_out.note_on(value, 127)
            sleep(.1)
            midi.midi_out.note_off(value, 127)
            sleep(.1)
